function getComp(comp: any) {
  return new Promise((resolve, reject) => {
    if (comp.name === 'AsyncComponentWrapper') {
      const timer = setInterval(() => {
        if (comp.__asyncResolved) {
          clearInterval(timer)
          resolve(comp.__asyncResolved)
        }
      }, 100)
    } else {
      resolve(comp)
    }
  })
}
export default {getComp}